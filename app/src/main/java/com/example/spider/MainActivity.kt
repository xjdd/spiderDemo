package com.example.spider

import android.app.Activity
import android.app.ActivityManager
import android.content.Context
import android.os.Bundle
import android.util.Log
import com.kding.spider.Base64Utils
import com.kding.spider.SpiderBuilder
import com.kding.spider.SpiderDoc
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.Exception

class MainActivity : Activity(), SpiderBuilder.SpiderCallback {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
//        val options = BitmapFactory.Options()
//        options.inPreferredConfig = Bitmap.Config.ARGB_8888
//        options.inSampleSize = 2
//        val bitmap = BitmapFactory.decodeResource(resources,R.mipmap.girl, options)
//        hello.text = ("${bitmap?.getByteCount()}")

        get.setOnClickListener {
            SpiderBuilder()
                .connectionTimeout(3000)
                .isHttps(true)
                .appKey("spider")
                .setCallBack(this)
                .connection("https://gitee.com/kding123/spider/blob/master/README.md")
                .get()
        }

        encode.setOnClickListener {
            edit.setText(Base64Utils.encodeHost(edit.text.toString()))
        }
        decode.setOnClickListener {
            edit.setText(Base64Utils.decodeHost(edit.text.toString()))
        }
        isMemoryAvail()


    }

    override fun onSuccess(pageTitle: String?, doc: SpiderDoc?) {
        get.text = pageTitle
    }

    override fun onError(e: Exception?) {
        Log.e("MainActivity", "Error ${e?.message}")
    }

    /**
     * 查看剩余内存是否超过阈值
     */
    fun isMemoryAvail() {
        val am = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val mi = ActivityManager.MemoryInfo()
        am.getMemoryInfo(mi)
        val memory = mi.availMem
        Log.e("MainActivity", "总内存= ${memory}")
    }
}
