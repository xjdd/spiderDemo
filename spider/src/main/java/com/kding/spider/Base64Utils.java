package com.kding.spider;

import android.util.Base64;

import java.nio.charset.Charset;


/**
 * Created by lishaojie on 2019/9/18
 * Base64解码
 */
public class Base64Utils {
    public static String decode(String code) {
        byte[] bytes = Base64.decode(code, 0);
        return new String(bytes, Charset.forName("UTF-8"));
    }

    public static String encode(String code) {
        return Base64.encodeToString(code.getBytes(), 0);
    }

    public static String encodeHost(String code) {
        String result = code
                .replace(".", "*")
                .replace("/", "%23");
        return encode(result) + "@";

    }

    public static String decodeHost(String origin) {
        return Base64Utils.decode(origin.split("@")[0])
                .replace("%3A",":")
                .replace("*", ".")
                .replace("%23", "/");

    }
}