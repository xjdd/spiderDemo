# 使用指南
## 增加依赖
implementation "com.pince.maven:spider:1.0.7"
##使用
```java
class MainActivity : AppCompatActivity(), SpiderCallback {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
      get.setOnClickListener {
                  SpiderBuilder()
                      .connectionTimeout(3000)
                       .isHttps(true)
                      .appKey("具体app对应的appkey")
                      .setCallBack(this)
                      .connection("https://gitee.com/kding123/spider/blob/master/README.md")
                      .get()
              }


    }

    override fun onSuccess(pageTitle: String?, doc: SpiderDoc?) {
       get.text = pageTitle
   }

   override fun onError(e: Exception?) {
       Log.e("MainActivity", "Error ${e?.message}")
   }
}
```
其中caBack方法中的 host 为爬取的网页的域名,可以通过 spiderDoc.doc获取到 Jsoup 的
Document对象，实现更为丰富的功能，具体见官方文档https://www.open-open.com/jsoup/

## 混淆配置
```
-keepclassmembers class org.jsoup.* {
   public *;
}
```
